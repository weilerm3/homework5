CC = gcc
C_FLAGS = -c
PGM = keeplog
HelperIncludePath = ~/include/keeplog_helper1.h
HelperIncludePath2 = ~/include/keeplog_helper2.h
all: $(PGM)

$(PGM): $(PGM).o
	$(CC) -o $(PGM) $(PGM).o -lkeeplog_helper1 -lkeeplog_helper2 -llist

$(PGM).o: $(PGM).c $(HelperIncludePath) $(HelperIncludePath2)
	$(CC) $(C_FLAGS) $(PGM).c

clean:
	@-rm $(PGM) *.o
